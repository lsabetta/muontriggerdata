# Descripption of the files

```
label_one_mu_NO_bkg.npy
image_one_mu_NO_bkg.npy
```

Original (n_images, 9, 384) shaped sample of images. "label_one_mu_NO_bkg.npy" is (n_images, 5) shaped, containin (pT, eta, phi, 0, nhits) where nhits is the number of pixels turned on by the muon.

```
train_image384_1mu_with_bkg_4gasgap.npy
test_image384_1mu_with_bkg_4gasgap.npy
```

Noise has been added (poissonian number of clusters, lambda=20, with a gaussian size extracted from the original sample) and sample has been splitted in training and test set (90%-10% respectively). The train images are selected to have at least a muon hit on at least 4 of the 9 different layers (criterion not applied to the test sample).

```
test_full_label64_1mu_with_bkg.npy
test_label64_1mu_with_bkg.npy
test_image64_1mu_with_bkg.npy
train_full_label64_1mu_with_bkg.npy
train_label64_1mu_with_bkg.npy
train_image64_1mu_with_bkg.npy
```

Images have been segmented into smaller images (9, 64) shaped (6 SmallImages plus 5 SmallImages representing the overlap between each separation = 11 SmallImages per (9,384) image). While in the training samplethese are randomly shuffled, in the test sample are ordered so that the original image can be rebuilt (first the first 6, then the 5 overlaps). "full_label" contains (p_t, eta_muon, eta_small, num_hits, p_t(always)). "label64" (pt, eta_small) only. The test sample also contains images with noise only (for which the target is (0, 0, 0, 0, 0))